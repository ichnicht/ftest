E

Exemplar är en enskild fysisk eller elektronisk representation av ett verk.

Exemplarframställning av verk är reglerad och skyddad genom lagar och avtal om upphovsrätt.

Svensk rätt

Upphovsmannen eller hans rättsinnehavare har uteslutande rätt att förfoga över verket genom att framställa exemplar av det och genom att göra det tillgängligt för allmänheten, i ursprungligt eller ändrat skick, i översättning eller bearbetning, i annan litteratur- eller konstart eller i annan teknik.[1]
Inskränkningar
Tillfälliga former

Tillfälliga former av exemplar av verk får framställas, om framställningen utgör en integrerad och väsentlig del i en teknisk process och om exemplaren är flyktiga eller har underordnad betydelse i processen. Exemplaren får inte ha självständig ekonomisk betydelse.

Framställning av exemplar är tillåten bara om det enda syftet med framställningen är att möjliggöra

    överföring i ett nät mellan tredje parter via en mellanhand, eller
    laglig användning, det vill säga användning som sker med tillstånd från upphovsmannen eller dennes rättsinnehavare, eller annan användning som inte är otillåten enligt denna lag.

    Första och andra styckena ger inte rätt att framställa exemplar av litterära verk i form av datorprogram eller sammanställningar.[2]

Privat bruk

Var och en får för privat bruk framställa ett eller några få exemplar av offentliggjorda verk. Såvitt gäller litterära verk i skriftlig form får exemplarframställningen dock endast avse begränsade delar av verk eller sådana verk av begränsat omfång. Exemplaren får inte användas för andra ändamål än privat bruk.

Första stycket ger inte rätt att

    uppföra byggnadsverk,
    framställa exemplar av datorprogram, eller
    framställa exemplar i digital form av sammanställningar i digital form.

Första stycket ger inte heller rätt att för privat bruk låta en utomstående

    framställa exemplar av musikaliska verk eller filmverk,
    framställa bruksföremål eller skulpturer, eller
    genom konstnärligt förfarande efterbilda andra konstverk.

Denna paragraf ger inte rätt att framställa exemplar av ett verk när det exemplar som är den egentliga förlagan framställts eller gjorts tillgängligt för allmänheten i strid med upphovsmannens rätt.[3]
Undervisningsändamål

För undervisningsändamål får lärare och elever göra upptagningar av sina egna framföranden av verk. Upptagningarna får inte användas för andra ändamål.[4]
För arkiv och bibliotek

    Statliga och kommunala arkivmyndigheterna,
    de vetenskapliga bibliotek och fackbibliotek som drivs av det allmänna, och
    folkbiblioteken har rätt att framställa exemplar av verk, dock inte datorprogram

    för bevarande-, kompletterings- eller forskningsändamål,
    för att tillgodose lånesökandes önskemål om enskilda artiklar eller korta avsnitt eller om material som av säkerhetsskäl inte bör lämnas ut i original, eller
    för användning i läsapparater.

Exemplar som framställs på papper får spridas till lånesökande.

Bestämmelser om avtalslicens för spridning av exemplar till allmänheten i andra fall och för överföring av verk till allmänheten finns i 42 a- d §.[5]
Till personer med funktionsnedsättning

Var och en får på annat sätt än genom ljudupptagning framställa sådana exemplar av offentliggjorda litterära och musikaliska verk samt av offentliggjorda alster av bildkonst, som personer med funktionsnedsättning behöver för att kunna ta del av verken.[6] 