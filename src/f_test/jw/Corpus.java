package f_test.jw;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.core.tokenizers.Tokenizer;
import weka.core.tokenizers.WordTokenizer;

/**
 * Data and processing class. Saves different stages of the corpus (= all documents) and does the job (tokenizing, calculating the score tf-idf, performing search)
 */
public class Corpus {


    private ArrayList<String> documents = new ArrayList<String>(); //raw text
    private List<List<String>> tokenizedDocuments = new ArrayList<>(); //the documents as a list of their component words
    private final static Logger LOGGER = Logger.getLogger(Corpus.class.getName());
    private Tokenizer tokenizer=new WordTokenizer();
    private HashMap<String, List<DocumentValue>> lexicon = new HashMap<String, List<DocumentValue>>(); //the index (words| documents containing that word and its score)

    /**
     * reads all text documents from file
     * @param filename text file, a line will be a search document
     * @throws IOException
     */
    public void init(String filename) throws IOException {


        //LOGGER.info("reading " + filename);
        BufferedReader reader;

            reader = new BufferedReader(new FileReader(
                    filename));
            String line = reader.readLine();
            while (line != null && !line.isEmpty()) {
                if(!documents.contains(line)) documents.add(line);
                line = reader.readLine();
            }
            reader.close();

    }


    public List<String> getDocuments() {
        return documents;
    }


    public List<List<String>> getTokenizedDocuments() {
        return tokenizedDocuments;
    }

    public HashMap<String, List<DocumentValue>> getLexicon() {
        return lexicon;
    }

    /**
     * splits up given documents into the containing words. Updates member variables lexicon and tokenizedDocuments
     * @param toLowerCase if true all words are added in lower case only.
     */
    public void tokenize(boolean toLowerCase) {
        HashMap<String, List<DocumentValue>> lex = new HashMap<String, List<DocumentValue>>();
        tokenizedDocuments = new ArrayList<List<String>>();

        for (String document : documents) {
            int documentId =  documents.indexOf(document);
            this.tokenizer.tokenize(document);
            List<String> currenTokenizedDocument = new ArrayList<>();
            while(this.tokenizer.hasMoreElements()) {
                String word = (String) this.tokenizer.nextElement();
                if (toLowerCase) word = word.toLowerCase();

                currenTokenizedDocument.add(word);

                if (!lex.containsKey(word)) {
                    List<DocumentValue> docList = new ArrayList<>();
                    docList.add(new DocumentValue(documentId));
                    lex.put(word, docList);
                }

                else {
                    boolean found = false;
                    for (DocumentValue d : lex.get(word)) {
                        if (d.getDocumentId() == documentId )
                         found = true;
                        break;
                    }
                    if (!found) lex.get(word).add(new DocumentValue(documentId)) ;
                }

            }
            tokenizedDocuments.add(documentId, currenTokenizedDocument);
        }
        lexicon = lex;


    }

    /**
     * for all words in the documents calculate tf-idf score and write it down in the member variable lexicon
     * @param toLowerCase if true transform all words into lower case
     */
    public void calculateTfIdf(boolean toLowerCase) {
        TFidfCalculator calculator = new TFidfCalculator();
        for  (String key :lexicon.keySet()) {
            for (DocumentValue documentValue : lexicon.get(key)) {


                double score = calculator.tfIdf(tokenizedDocuments.get(documentValue.getDocumentId()), tokenizedDocuments, key, toLowerCase);
                documentValue.setScore(score);
            }

        }
    }

    /**
     * perform a search in the given corpus
     * @param word
     * @param toLowerCase if true transform all words into lower case
     * @param descendendingSortOrder if true highest document score first! Should be default.
     * @return a list with document ids that contain the search term sorted by relevance, i.e. the more frequent the search term is, the higher the score tf-idf
     */
    public List<DocumentValue> search(String word, boolean toLowerCase, boolean descendendingSortOrder) {
        if(word== null || word.isEmpty()) {
            LOGGER.log(Level.SEVERE , "the input word is empty or null " + word);
            return null;
        }

        if(lexicon== null || lexicon.isEmpty()) {
            LOGGER.log(Level.SEVERE , "the lexicon is empty. Did you run the init function correctly? ");
            return null;
        }

        String searchFor = word;
        if (toLowerCase) searchFor = searchFor.toLowerCase();
        List<DocumentValue> result = lexicon.get(searchFor);

        if(result == null || result.isEmpty()) {
            LOGGER.log(Level.ALL, "no search results found for input word " + searchFor);
            return null;
        }
        Comparator<DocumentValue> comp = new DocumentValue.SortDocument();
        if (descendendingSortOrder) comp = new DocumentValue.SortDocumentReverse();
        result.sort(comp);
        return result;
    }

    /**
     * transforms search result into a human readable list. Document Id is added by 1 to make it start by 1 and not by 0.
     * @param searchResult
     * @return a string like "[document 1, document 2]"
     */
    public static String searchResultToString(List<DocumentValue> searchResult) {
        String ret = "[";
        for(DocumentValue d : searchResult) {
            ret += "document " + (d.getDocumentId() +1) + ", ";
        }
        //remove last space and comma
        ret = ret.substring(0, ret.length()-2);
        ret += "]";
        return ret;
    }


}
