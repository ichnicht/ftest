package f_test.jw;

import java.util.Comparator;

/**
 * Data class that reflects the indexed document, i.e. the document ID and its score.
 */
public class DocumentValue {
    private int documentId; //position in corpus.documents
    private double score; //tf-idf value for this term in this document

    public DocumentValue(int documentId) {
        this.documentId = documentId;
        this.score = 0;
    }

    public static class SortDocument implements Comparator<DocumentValue> {

        @Override
        public int compare(DocumentValue o1, DocumentValue o2) {
            if (o1.getScore() > o2.getScore()) return 1;
            if (o1.getScore() == o2.getScore()) return 0;
            else //if (o1.getScore() < o2.getScore())
                return -1;
        }
    }

    public static class SortDocumentReverse implements Comparator<DocumentValue> {

        @Override
        public int compare(DocumentValue o1, DocumentValue o2) {
            if (o1.getScore() > o2.getScore()) return -1;
            if (o1.getScore() == o2.getScore()) return 0;
            else //if (o1.getScore() < o2.getScore())
                return 1;
        }
    }

    public int getDocumentId() {
        return documentId;
    }

    public void setDocumentId(int documentId) {
        this.documentId = documentId;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public String toString() {
        return "documentId: " + documentId + " score: " + score;
    }
}


