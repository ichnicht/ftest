package f_test.jw;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

public class Main {
    private final static Logger LOGGER = Logger.getLogger(Main.class.getName());
    public static void main(String[] args) {
        String userInput;
        String filename = "";

        Scanner sn = new Scanner(System.in);



        System.out.println("*****Very simple search engine*****");
        System.out.println("*****Search is case insensitive, i.e. Dog = dog *****");
        System.out.println("Please enter a file name containing documents or press 1 for exempelUppgift.txt or 2 for exempel.txt");
        System.out.println("Press q to exit");
        // Prompt the use to make a choice
        System.out.println("Enter your choice:");


        userInput = sn.next();


        switch (userInput) {
            case "1":
                filename = "exempelUppgift.txt";
                break;
            case "2":
                filename = "exempel.txt";
                break;
            case "q":
                //exit from the program
                System.out.println("Exiting...");
                System.exit(0);
            default:
                filename = userInput;
        }
        Corpus corpus = new Corpus();
        try {
            System.out.println("Reading from file " + filename);
            corpus.init(filename);
            corpus.tokenize(true);
            corpus.calculateTfIdf(true);
            System.out.println(corpus.getTokenizedDocuments().size() + " documents found.");
            System.out.println("Initialization done.");
        } catch (IOException e) {

            LOGGER.severe("File not found or another problem with the file with filename " + filename);
            LOGGER.severe(e.getLocalizedMessage());
        }

        while(true) {




            System.out.println("Enter your search term or q to quit:");
            String searchterm = sn.next();

            switch (searchterm) {

                case "q":
                    //exit from the program
                    System.out.println("Exiting...");
                    System.exit(0);
                default:
                    System.out.println("search term is " + searchterm);
            }


            List<DocumentValue> s = corpus.search(searchterm, true, false);
            if (s == null)  {
                System.out.println("No results found for search term " + searchterm);
                continue;
            }

            System.out.println("Found results: " + Corpus.searchResultToString(s));




        }

    }
}
