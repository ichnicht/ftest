package f_test.jw;

import java.util.List;
import java.util.logging.Logger;

/**
 * Stolen from the internet
 * @author Mohamed Guendouz
 * https://gist.github.com/guenodz/d5add59b31114a3a3c66
 * I adapted this class and added unit tests and the boolean parameter toLowerCase
 */
public class TFidfCalculator {

    private final static Logger LOGGER = Logger.getLogger(TFidfCalculator.class.getName());

    /**
     * @param doc  list of strings
     * @param term String represents a term
     * @param toLowerCase if true all words are convereted to lower case, (The = the)
     * @return term frequency of term in document
     */
    public double tf(List<String> doc, String term, boolean toLowerCase) {
        double result = 0;
        for (String word : doc) {
            String rightCaseWord = word;
            if (toLowerCase) rightCaseWord = rightCaseWord.toLowerCase();

            if (term.equals(rightCaseWord))
                result++;

        }
        return result / doc.size();
    }

    /**
     * @param docs list of list of strings represents the dataset
     * @param term String represents a term
     * @param toLowerCase if true all words are convereted to lower case, (The = the)
     * @return the inverse term frequency of term in documents
     */
    public double idf(List<List<String>> docs, String term, boolean toLowerCase) {
        double n = 0;
        for (List<String> doc : docs) {
            for (String word : doc) {
                String rightCaseWord = word;
                if (toLowerCase) rightCaseWord = rightCaseWord.toLowerCase();
                if (term.equals(rightCaseWord)) {
                    n++;
                    break;
                }
            }
        }
       return Math.log(docs.size() / n);
    }

    /**
     * @param doc  a text document
     * @param docs all documents
     * @param term term
     * @param toLowerCase if true all words are convereted to lower case, (The = the)
     * @return the TF-IDF of term
     */
    public double tfIdf(List<String> doc, List<List<String>> docs, String term, boolean toLowerCase) {
        return tf(doc, term, toLowerCase) * idf(docs, term, toLowerCase);

    }




}