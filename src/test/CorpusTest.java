package test;

import f_test.jw.Corpus;
import f_test.jw.DocumentValue;

import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.*;

/**
 * unit tests for corpus class. naming according to given_when_then
 */
public class CorpusTest {
    private final static Logger LOGGER = Logger.getLogger(CorpusTest.class.getName());


    @Test
    public void documents_testInit_3found() throws IOException {
        Corpus testCorpus = new Corpus( );
        testCorpus.init("exempelUppgift.txt");
        assertEquals(3, testCorpus.getDocuments().size());
    }

    @Test
    public void documents_tokenize_14termsfound() throws IOException {
        Corpus testCorpus = new Corpus( );
        testCorpus.init("exempelUppgift.txt");
        testCorpus.tokenize(false);
        HashMap<String, List<DocumentValue>> testLex = testCorpus.getLexicon();
        assertFalse(testLex.isEmpty());
        assertEquals(14, testLex.size());
    }

    @Test
    public void documents_tokenizeToLowerCase_12termsfound() throws IOException {
        Corpus testCorpus = new Corpus( );
        testCorpus.init("exempelUppgift.txt");
        testCorpus.tokenize(true);
        HashMap<String, List<DocumentValue>> testLex = testCorpus.getLexicon();
        assertFalse(testLex.isEmpty());
        assertEquals(12, testLex.size());
    }

    @Test
    public void documents_tokenize_3foundTokenizedDocuments() throws IOException {
        Corpus testCorpus = new Corpus( );
        testCorpus.init("exempelUppgift.txt");
        testCorpus.tokenize(true);
        List<List<String>> tok = testCorpus.getTokenizedDocuments();
        assertFalse(tok.isEmpty());
        assertEquals(3, tok.size());
    }

    @Test
    public void wordDog_getDocumentIds_3occurences() throws IOException {
        Corpus testCorpus = new Corpus( );
        testCorpus.init("exempelUppgift.txt");
        testCorpus.tokenize(false);
        HashMap<String, List<DocumentValue>> testLex = testCorpus.getLexicon();
        assertEquals(3, testLex.get("dog").size());
    }


    @Test
    public void searchString_toString_readableString() {
        //input value: [documentId: 0 score: 0.0, documentId: 1 score: 0.0, documentId: 2 score: 0.0]
        List<DocumentValue> result = new ArrayList<>();
        for (int i = 0; i < 3; i++)
            result.add(new DocumentValue(i));
        String expected = "[document 1, document 2, document 3]";
        String whatIGet = Corpus.searchResultToString(result);
        assertTrue(expected.equals(whatIGet));
    }


    @Test
    public void wordbrown_searchinCorpus_readableResult() throws IOException {
        boolean toLowerCase = false;
        Corpus testCorpus = new Corpus( );
        testCorpus.init("exempelUppgift.txt");
        testCorpus.tokenize(toLowerCase);
        testCorpus.calculateTfIdf(toLowerCase);
        LOGGER.info("Lexicon " + testCorpus.getLexicon());
        List<DocumentValue> result = testCorpus.search("brown", toLowerCase, true);
        LOGGER.info("tokenized corpus " + testCorpus.getTokenizedDocuments());
        LOGGER.info(""+result);

        assertEquals("[document 1, document 2]", Corpus.searchResultToString(result));
    }


    @Test
    public void wordfox_searchinCorpus_readableResult() throws IOException {
        boolean toLowerCase = true;
        Corpus testCorpus = new Corpus( );
        testCorpus.init("exempelUppgift.txt");
        testCorpus.tokenize(toLowerCase);
        testCorpus.calculateTfIdf(toLowerCase);
        LOGGER.info("Lexicon " + testCorpus.getLexicon());
        List<DocumentValue> result = testCorpus.search("fox", toLowerCase, true);
        LOGGER.info("tokenized corpus " + testCorpus.getTokenizedDocuments());
        LOGGER.info(""+result);

        assertEquals("[document 3, document 1]", Corpus.searchResultToString(result));
    }


    @Test
    //the expected solution is [document 3, document 2, document 1]
    //My algorithm returns [document 1, document 2, document 3]
    //The word dog appears in each document. the score is 0.0 for all documents so the order is insignificant.
    public void wordDog_searchinCorpus_readableResult() throws IOException {
        boolean toLowerCase = false;
        Corpus testCorpus = new Corpus( );
        testCorpus.init("exempelUppgift.txt");
        testCorpus.tokenize(toLowerCase);
        testCorpus.calculateTfIdf(toLowerCase);
        LOGGER.info("Lexicon " + testCorpus.getLexicon());
        List<DocumentValue> result = testCorpus.search("dog", toLowerCase, true);
        LOGGER.info("tokenized corpus " + testCorpus.getTokenizedDocuments());
        LOGGER.info(""+result);
        String resultString = Corpus.searchResultToString(result);
        assertTrue(resultString.contains("document 3"));
        assertTrue(resultString.contains("document 2"));
        assertTrue(resultString.contains("document 1"));
        assertEquals(3, result.size());

    }





}