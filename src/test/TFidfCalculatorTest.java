package test;

import f_test.jw.TFidfCalculator;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * tests for class tfidf calculator. naming according to given_when_then
 */
public class TFidfCalculatorTest {

    @Test
    //given example in class
    public void Exampleipsum_tFidflowerCase_score() {
        List<String> doc1 = Arrays.asList("Lorem", "ipsum", "dolor", "ipsum", "sit", "ipsum");
        List<String> doc2 = Arrays.asList("Vituperata", "incorrupte", "at", "ipsum", "pro", "quo");
        List<String> doc3 = Arrays.asList("Has", "persius", "disputationi", "id", "simul");
        List<List<String>> documents = Arrays.asList(doc1, doc2, doc3);

        TFidfCalculator calculator = new TFidfCalculator();
        double tfidf = calculator.tfIdf(doc1, documents, "ipsum", true);
        double expected = 0.2027325540540822;
        double expectedtf = 0.5;
        double expecteditf = 0.4054651081081644;
        assertEquals(expectedtf, calculator.tf(doc1, "ipsum", false));
        assertEquals(expecteditf, calculator.idf(documents, "ipsum", false));
        assertEquals(expected, tfidf);
    }


    //given example in test
    @Test
    public void exampletest_tFidfNotLowerCase_score() {
        List<String> doc1 = Arrays.asList("The", "brown", "fox", "jumped", "over", "the", "brown", "dog");
        List<String> doc2 = Arrays.asList("The", "lazy", "brown", "dog", "sat", "in", "the", "corner");
        List<String> doc3 = Arrays.asList("The", "Red", "Fox", "bit", "the", "lazy", "dog");
        List<List<String>> documents = Arrays.asList(doc1, doc2, doc3);

        TFidfCalculator calculator = new TFidfCalculator();
        double tfidf = calculator.tfIdf(doc1, documents, "brown", false);
        double expected = 0.1013662770270411;
        assertEquals(expected, tfidf);
    }

    @Test
    //the word dog appears in each document, it has no information value for the 'specificness' of a certain document
    public void word_dog_tfidf_zero() {
        List<String> doc1 = Arrays.asList("The", "brown", "fox", "jumped", "over", "the", "brown", "dog");
        List<String> doc2 = Arrays.asList("The", "lazy", "brown", "dog", "sat", "in", "the", "corner");
        List<String> doc3 = Arrays.asList("The", "Red", "Fox", "bit", "the", "lazy", "dog");
        List<List<String>> documents = Arrays.asList(doc1, doc2, doc3);

        TFidfCalculator calculator = new TFidfCalculator();
        double tfidf = calculator.tfIdf(doc1, documents, "dog", true);
        double expected = 0.0;
        assertEquals(expected, tfidf);
    }

}